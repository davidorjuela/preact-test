import { h } from 'preact';
import { useState } from 'preact/hooks'
import Flicking from '@egjs/preact-flicking';
import "@egjs/preact-flicking/dist/flicking.css";
import './style.css';

const Panel = () => {

	const [panels, setPanels] = useState([0, 1, 2, 3, 4]);

	return <>
		<div class={'home'}>
			<h1>Panel</h1>
			<Flicking>
				<div class={'panel'}>1</div>
				<div class={'panel' +" "+ 'nested'}>
					<Flicking bounce="0" bound={true} nested={true}>
						<div class={'panel'}>2.1</div>
						<div class={'panel'}>2.2</div>
						<div class={'panel'}>2.3</div>
					</Flicking>
				</div>
				<div class={'panel'+" "+'nested'+" "+'vertical'}>
					<Flicking bounce="0" bound={true} horizontal={false}>
						<div class={'panel'}>3.1</div>
						<div class={'panel'}>3.2</div>
						<div class={'panel'}>3.3</div>
					</Flicking>
				</div>
				<div class={'panel'}>4</div>
			</Flicking>
		</div>
	</>
};

export default Panel;
