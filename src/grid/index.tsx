import { h } from 'preact';
import { useState, useEffect } from 'preact/hooks'
//import Flicking from '@egjs/preact-flicking';
//import "@egjs/preact-flicking/dist/flicking.css";
import { MasonryGrid, JustifiedGrid, FrameGrid, PackingGrid } from "@egjs/grid";
import './style.css';


const Panel = () => {

	useEffect(() => {

		const container = document.getElementById('container')||''

		const grid = new MasonryGrid(container, {
			defaultDirection: "end",
			gap: 5,
			useResizeObserver: true,
			observeChildren: true,
			align: "justify",
			column: 0,
			columnSize: 0,
			columnSizeRatio: 0,
		});

		grid.renderItems();

	})

	return (
		<>
			<h1>Grid</h1>
			<div id="container" class={'container'}>
				<div class={'item'}>1</div>
				<div class={'item'}>2</div>
				<div class={'item'}>3</div>
				<div class={'item'}>4</div>
				<div class={'item'}>5</div>
				<div class={'item'}>6</div>
				<div class={'item'}>7</div>
				<div class={'item'}>8</div>
				<div class={'item'}>9</div>
				<div class={'item'}>10</div>
			</div>
		</>

	)
};

export default Panel;
