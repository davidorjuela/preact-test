import Panel from './panel'
import Grid from './grid'

const App = () => {

    return (
        <>
            <Panel />
            <Grid />
        </>
    )
}

export default App